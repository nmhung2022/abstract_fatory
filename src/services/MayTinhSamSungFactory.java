package services;

import keyboard.BanPhim;
import keyboard.BanPhimSamSungHigh;
import keyboard.BanPhimSamSungIntermediate;
import screen.ManHinh;
import screen.ManHinhSamSungHigh;
import screen.ManHinhSamSungIntermediate;

public class MayTinhSamSungFactory extends MayTinhAbstractFactory {

	@Override
	public BanPhim sanXuatBanPhim() {
		// TODO Auto-generated method stub
		// Logic tạo bàn phím sam sung ở nhiều phân khúc khác nhau

		int min = 1;
		int max = 10;

		int random_int = (int) Math.floor(Math.random() * (max - min + 1) + min);
		if (random_int > 5) {
			return new BanPhimSamSungIntermediate();
		} else {
			return new BanPhimSamSungHigh();
		}

	}

	@Override
	public ManHinh sanXuatManHinh() {
		// TODO Auto-generated method stub
		// Logic tạo màn hình sam sung ở nhiều phân khúc khác nhau

		int min = 1;
		int max = 10;

		int random_int = (int) Math.floor(Math.random() * (max - min + 1) + min);
		if (random_int > 5) {
			return new ManHinhSamSungHigh();
		} else {
			return new ManHinhSamSungIntermediate();
		}
	}

}
