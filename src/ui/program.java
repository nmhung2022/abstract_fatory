package ui;

import keyboard.BanPhim;
import screen.ManHinh;
import services.MayTinhAbstractFactory;
import services.MayTinhFactory;

public class program {

	public static void main(String[] args) {
		MayTinhAbstractFactory factory = MayTinhFactory.getFactory();
		BanPhim keyboard = factory.sanXuatBanPhim();
		keyboard.taoBanPhim();
		System.out.println("----------------------------------");
		ManHinh screen = factory.sanXuatManHinh();
		screen.taoManHinh();
	}

}
